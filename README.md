
### Simulation of a fight between a hero and a monster 

Prerequisites:
- PHP >=8.0
- Composer >=2

Instructions:
- `composer install` inside the directory
- run `php /src/index.php` to see the simulation run
- run `./vendor/bin/phpunit --testdox` to run the test with a nice output :)