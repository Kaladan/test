<?php

use Interview\Testing\Monster;
use Interview\Testing\Narrator;
use Interview\Testing\Player;
use PHPUnit\Framework\TestCase;

class NarratorTest extends TestCase
{
    public function testBeginningText()
    {
        $expected = "The hero Orderus enters the Dark Forest in hopes of finding what he was tasked to find. \nHis mission will not be an easy one, as the forest if famed to be full of dangerous creatures, \nbut his heart is true and so is his resolve: he will find his lords missing cat! \n\n";
        $this->assertEquals($expected, Narrator::begin());
    }

    public function testStartingStatsText()
    {
        $player = new Player(10, 10, 10, 10, 10);
        $expected = "\033[32mOrderus starts out the turn with {$player->health} HP ({$player->strength} STR, {$player->speed} SP, {$player->defence} DEF) \n";
        $this->assertEquals($expected, Narrator::startingStats($player));
    }


    public function testFirstToAttackText()
    {
        $player = new Player(10, 10, 10, 10, 10);
        $expected = "\033[32mOrderus is attaching. Monster will be on the defence. \n\n";
        $this->assertEquals($expected, Narrator::firstToAttack($player));
    }

    public function testTurnText()
    {
        $expected = "\033[32mStarting turn 5\n";
        $this->assertEquals($expected, Narrator::turn(5));
    }

    public function testPlayerAttackText()
    {
        $player = new Player(10, 10, 10, 10, 10);
        $expected = <<<EOT
            \033[31mHero has attacked. Monster is defending. Damage dealt is 5. Monster health is 0.";
            \033[31mMonster has lost this fight;
            \033[32mHero has won this fight;
            EOT;

        $this->assertEquals($expected, Narrator::characterAttacks($player, 5));

        $monster = new Monster(10, 10, 10, 10, 10);
        $expected = <<<EOT
            \033[31mMonster has attacked. Hero is defending. Damage dealt is 5. Hero health is 0.";
            \033[31mHero has lost this fight;
            \033[32mMonster has won this fight;
            EOT;

        $this->assertEquals($expected, Narrator::characterAttacks($monster, 5));
    }

    public function testPlayerAttacksTwiceText()
    {
        $expected = "Hero has attacked twice this turn. \n";
        $this->assertEquals($expected, Narrator::playerAttacksTwice());
    }

    public function testPlayerDeflectsText()
    {
        $expected = "Hero has deflected this turn. Damage dealt to hero is 5. \n";
        $this->assertEquals($expected, Narrator::playerDeflects(5));
    }

    public function testPlayerAttacksText()
    {
        $expected = "Hero has attacked. Monster is defending. Damage dealt is 5. Remaining monster health is 5 \n\n";
        $this->assertEquals($expected, Narrator::playerAttacks(5, 5));
    }

    public function testMonsterAttacksText()
    {
        $expected = "Monster has attacked. Hero is defending. Damage dealt is 5. Remaining hero health is 5 \n\n";
        $this->assertEquals($expected, Narrator::monsterAttacks(5, 5));
    }

    public function testWhoWinsByHealthStatsText()
    {
        $player = new Player(12, 10, 10, 10, 10);
        $monster = new Monster(10, 10, 10, 10, 10);
        $expected = "\033[32mHero has won this fight \n\n";
        $this->assertEquals($expected, Narrator::winsByHealthStats($player, $monster));
    }
}
