<?php


use Interview\Testing\FightEngine;
use Interview\Testing\Monster;
use Interview\Testing\Player;
use PHPUnit\Framework\TestCase;

class FightEngineTest extends TestCase
{
    private int $playerHealth = 85;
    private int $playerStrength = 75;
    private int $playerDefence = 50;
    private int $playerSpeed = 45;
    private int $playerLuck= 25;

    private int $monsterHealth = 80;
    private int $monsterStrength = 70;
    private int $monsterDefence = 45;
    private int $monsterSpeed = 42;
    private int $monsterLuck = 27;
    private FightEngine $fightEngine;

    public function setUp(): void
    {
        parent::setUp();

        $this->fightEngine = new FightEngine(
            new Player($this->playerHealth, $this->playerStrength, $this->playerDefence, $this->playerSpeed, $this->playerLuck),
            new Monster($this->monsterHealth, $this->monsterStrength, $this->monsterDefence, $this->monsterSpeed, $this->monsterLuck),
        );
    }

    public function testGetFirstAttackerWhenSpeedIsDifferent()
    {
        $this->assertInstanceOf(Player::class, $this->fightEngine->decideAttackOrder());
    }

    public function testGetFirstAttackerWhenSpeedIsSame()
    {
        $this->playerSpeed = 50;
        $this->monsterSpeed = 50;

        $player = new Player($this->playerHealth, $this->playerStrength, $this->playerDefence, $this->playerSpeed, $this->playerLuck);
        $monster = new Monster($this->monsterHealth, $this->monsterStrength, $this->monsterDefence, $this->playerSpeed, $this->monsterLuck);

        $fightEngine = new FightEngine($player, $monster);

        $this->assertInstanceOf(Monster::class, $fightEngine->decideAttackOrder());
    }

    public function testFightResultMonsterShouldLoseAndPlayerAndMonsterAttacksShouldAlternate()
    {
        $result = $this->fightEngine->start(50, 50);
        $this->assertInstanceOf(Player::class, $result['attacker']);
        $this->assertInstanceOf(Monster::class, $result['defender']);
        $this->assertGreaterThan(0, $result['defender']->health);

        $result = $this->fightEngine->start(50, 50);
        $this->assertInstanceOf(Monster::class, $result['attacker']);
        $this->assertInstanceOf(Player::class, $result['defender']);
        $this->assertGreaterThan(0, $result['defender']->health);

        $result = $this->fightEngine->start(50, 50);
        $this->assertInstanceOf(Player::class, $result['attacker']);
        $this->assertInstanceOf(Monster::class, $result['defender']);
        $this->assertGreaterThan(0, $result['defender']->health);

        $result = $this->fightEngine->start(50, 50);
        $this->assertInstanceOf(Monster::class, $result['attacker']);
        $this->assertInstanceOf(Player::class, $result['defender']);
        $this->assertGreaterThan(0, $result['defender']->health);

        $result = $this->fightEngine->start(50, 50);
        $this->assertInstanceOf(Player::class, $result['attacker']);
        $this->assertInstanceOf(Monster::class, $result['defender']);
        $this->assertLessThan(0, $result['defender']->health);
    }

    public function testFightResultPlayerAttacksTwice()
    {
        $result = $this->fightEngine->start(50, 8);
        $this->assertInstanceOf(Player::class, $result['attacker']);
        $this->assertInstanceOf(Monster::class, $result['defender']);
        $this->assertGreaterThan(0, $result['defender']->health);
        $this->assertTrue($result['attackedTwice']);
    }

    public function testFightResultPlayerDefendsAndHisHealthTakesNoDamage()
    {
        $result1 = $this->fightEngine->start(15, 50);
        $this->assertInstanceOf(Player::class, $result1['attacker']);
        $this->assertInstanceOf(Monster::class, $result1['defender']);
        $this->assertGreaterThan(0, $result1['defender']->health);
        $this->assertFalse($result1['defended']);

        $result2 = $this->fightEngine->start(15, 50);
        $this->assertInstanceOf(Monster::class, $result2['attacker']);
        $this->assertInstanceOf(Player::class, $result2['defender']);
        $this->assertGreaterThan(0, $result2['defender']->health);
        $this->assertTrue($result2['defended']);

        $this->assertEquals($result1['attacker']->health, $result2['defender']->health);
    }
}
