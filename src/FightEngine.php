<?php

namespace Interview\Testing;

use Interview\Testing\Player;
use Interview\Testing\Monster;
use phpDocumentor\Reflection\Types\Boolean;

class FightEngine
{
    private Monster|Player $attacker;
    private Monster|Player $defender;
    private Monster|Player $currentAttacker;

    public function __construct(
        private Player $player,
        private Monster $monster,
    ) { }

    public function start(int $heroChanceToDefend, int $heroChangeToAttackTwice): array
    {
        $damage = 0;
        $defended = false;
        $attackedTwice = false;

        if (empty($this->currentAttacker)) {
            $this->currentAttacker = $this->decideAttackOrder();
        } else {
            $this->attacker = $this->currentAttacker instanceof Player ? $this->monster : $this->player;
            $this->defender = $this->currentAttacker instanceof Player ? $this->player : $this->monster;

            $this->currentAttacker = $this->attacker;
        }

        if (!$this->isDefenderPlayer()) {
            $damage = $this->attack();
        }

        if ($this->isDefenderPlayer()) {
            if ($heroChanceToDefend > 20) {
                $damage = $this->attack();
            } else {
                $defended = true;
            }
        }

        if ($heroChangeToAttackTwice < 10 && $this->attacker instanceof Player) {
            $damage = $this->attack() * 2;
            $attackedTwice = true;
        }

        return [
            'damage'   => $damage,
            'defended' => $defended,
            'attacker' => $this->attacker,
            'defender' => $this->defender,
            'attackedTwice' => $attackedTwice,
        ];
    }

    public function decideAttackOrder(): Player|Monster
    {
        if ($this->player->speed === $this->monster->speed) {
            $this->attacker = $this->player->luck > $this->monster->luck ? $this->player : $this->monster;
        }

        $this->attacker = $this->player->speed > $this->monster->speed ? $this->player : $this->monster;
        $this->defender = $this->attacker instanceof Player ? $this->monster : $this->player;

        return $this->attacker;
    }

    private function attack(): int
    {
        $damage = $this->attacker->strength - $this->defender->defence;

        if ($this->isDefenderPlayer()) {
            $this->player->health = $this->player->health - $damage;
        } else {
            $this->monster->health = $this->monster->health - $damage;
        }

        return $damage;
    }

    public function isDefenderPlayer(): bool
    {
        return $this->defender instanceof Player;
    }
}