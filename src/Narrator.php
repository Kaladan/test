<?php

namespace Interview\Testing;

class Narrator
{
    CONST COLOR_RED = "\033[31m";
    CONST COLOR_GREEN = "\033[32m";

    public static function begin(): string
    {
        return "The hero Orderus enters the Dark Forest in hopes of finding what he was tasked to find. \nHis mission will not be an easy one, as the forest if famed to be full of dangerous creatures, \nbut his heart is true and so is his resolve: he will find his lords missing cat! \n\n";
    }

    public static function startingStats(Player|Monster $character): string
    {
        $characterName = $character instanceof Player ? "Orderus" : "Monster";

        return self::COLOR_GREEN . $characterName . " starts out the turn with {$character->health} HP ({$character->strength} STR, {$character->speed} SP, {$character->defence} DEF) \n";
    }

    public static function firstToAttack(Player|Monster $character): string
    {
        $attacker = $character instanceof Player ? "Orderus" : "Monster";
        $defender = $character instanceof Player ? "Monster" : "Orderus";

        return self::COLOR_GREEN . $attacker . " is attaching. $defender will be on the defence. \n\n";
    }

    public static function turn(int $turn): string
    {
        return self::COLOR_GREEN . "Starting turn " . $turn . "\n";
    }

    public static function characterAttacks($character, int $damage): string
    {
        $attacker = $character instanceof Player ? "Hero" : "Monster";
        $defender = $character instanceof Player ? "Monster" : "Hero";

        return <<<EOT
            \033[31m$attacker has attacked. $defender is defending. Damage dealt is $damage. $defender health is 0.";
            \033[31m$defender has lost this fight;
            \033[32m$attacker has won this fight;
            EOT;
    }

    public static function playerAttacksTwice(): string
    {
        return "Hero has attacked twice this turn. \n";
    }

    public static function playerDeflects(int $damage): string
    {
        return "Hero has deflected this turn. Damage dealt to hero is {$damage}. \n";
    }

    public static function playerAttacks(int $damage, int $remainingMonsterHealth): string
    {
        return "Hero has attacked. Monster is defending. Damage dealt is {$damage}. Remaining monster health is {$remainingMonsterHealth} \n\n";
    }

    public static function monsterAttacks(int $damage, int $remainingPlayerHealth): string
    {
        return "Monster has attacked. Hero is defending. Damage dealt is {$damage}. Remaining hero health is {$remainingPlayerHealth} \n\n";
    }

    public static function winsByHealthStats(Player|Monster $attacker, Player|Monster $defender): string
    {
        $character = $attacker instanceof Player ? "Hero" : "Monster";
        return $attacker->health > $defender->health
            ? self::COLOR_GREEN . "{$character} has won this fight \n\n"
            : self::COLOR_GREEN . "{$character} has won this fight \n\n";
    }
}
