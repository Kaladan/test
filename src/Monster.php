<?php

namespace Interview\Testing;

class Monster
{
    public function __construct(
        public int $health,
        public int $strength,
        public int $defence,
        public int $speed,
        public int $luck,
    ) { }

}