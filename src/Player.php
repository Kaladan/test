<?php

namespace Interview\Testing;

class Player
{
    public function __construct(
        public int $health,
        public int $strength,
        public int $defence,
        public int $speed,
        public int $luck,
    ) { }
}