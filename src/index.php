<?php

use Interview\Testing\FightEngine;
use Interview\Testing\Monster;
use Interview\Testing\Narrator;
use Interview\Testing\Player;

require_once __DIR__ . '/../vendor/autoload.php';

echo "\n\n";
echo Narrator::begin();

$player = new Player(
    $health = rand(70, 100),
    $strength = rand(70, 80),
    $defence = rand(45, 55),
    $speed = rand(40, 50),
    $luck = rand(10, 30)
);
$monster = new Monster(
    $health = rand(60, 90),
    $strength = rand(60, 90),
    $defence = rand(40, 60),
    $speed = rand(40, 60),
    $luck = rand(25, 40)
);

echo Narrator::startingStats($player);
echo Narrator::startingStats($monster);

$fight = (new FightEngine($player, $monster));

$attacker = $fight->decideAttackOrder();

echo Narrator::firstToAttack($attacker);

for ($i = 0; $i < 20; $i++) {
    echo Narrator::turn($i + 1);

    $result = $fight->start(mt_rand(0, 100), mt_rand(0, 100));

    $remainingMonsterHealth = $result['attacker'] instanceof Player ? $result['defender']->health : $result['attacker']->health;
    $remainingPlayerHealth = $result['attacker'] instanceof Monster ? $result['defender']->health : $result['attacker']->health;

    if ($result['attacker'] instanceof Player) {
        if ($result['defender']->health <= 0) {
            echo Narrator::characterAttacks($result['attacker'], $result['damage']);
            break;
        }

        if ($result['attackedTwice']) {
            echo Narrator::playerAttacksTwice();
        }

        if ($result['defended']) {
            echo Narrator::playerDeflects($result['damage']);
        }

        echo Narrator::playerAttacks($result['damage'], $remainingMonsterHealth);
    }

    if ($result['attacker'] instanceof Monster) {
        if ($result['defender']->health <= 0) {
            echo Narrator::characterAttacks($result['attacker'], $result['damage']);
            break;
        }

        echo Narrator::monsterAttacks($result['damage'], $remainingPlayerHealth);
    }

    if ($i === 19) {
        echo Narrator::winsByHealthStats($result['attacker'], $result['defender']);
    }
}

echo PHP_EOL;